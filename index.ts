import axios from 'axios';

class GoogleTTS {
	private host: string;
	private keys?: string;

	constructor() {
		this.host = 'https://translate.google.com';
	}

	private async fetchKeys(timeout: number) {
		if (this.keys) return this.keys;
		const response = await axios.get(this.host, { timeout, responseType: 'document' });
		const matches = ["TKK='(\\d+.\\d+)';", "tkk:'(\\d+.\\d+)'"].map(expr => response.data.match(expr)).filter(res => res);
		if (!matches.length) throw new Error('Failed to get keys from Google.');
		const keys = this.keys = matches[0][1];
		return keys;
	}

	private XL(x: any, y: any) {
		for (let c = 0; c < y.length - 2; c += 3) {
			let d = y.charAt(c + 2);
			d = d >= 'a' ? d.charCodeAt(0) - 87 : Number(d);
			d = y.charAt(c + 1) == '+' ? x >>> d : x << d;
			x = y.charAt(c) == '+' ? x + d & 4294967295 : x ^ d;
		}
		return x;
	}
	  
	private token(text: any, key: any) {
		let a = text, b = key, d = b.split('.');
		b = Number(d[0]) || 0;
		let e = [];
		for (let f = 0, g = 0; g < a.length; g++) {
			let m = a.charCodeAt(g);
			128 > m ? e[f++] = m : (2048 > m ? e[f++] = m >> 6 | 192 : (55296 == (m & 64512) && g + 1 < a.length && 56320 == (a.charCodeAt(g + 1) & 64512) ? (m = 65536 + ((m & 1023) << 10) + (a.charCodeAt(++g) & 1023),
			e[f++] = m >> 18 | 240,
			e[f++] = m >> 12 & 63 | 128) : e[f++] = m >> 12 | 224,
			e[f++] = m >> 6 & 63 | 128),
			e[f++] = m & 63 | 128);
		}
		a = b;
		for (let f = 0; f < e.length; f++) {
			a += e[f];
			a = this.XL(a, '+-a^+6');
		}
		a = this.XL(a, '+-3^+b+-f');
		a ^= Number(d[1]) || 0;
		0 > a && (a = (a & 2147483647) + 2147483648);
		a = a % 1E6;
		return a.toString() + '.' + (a ^ b);
	};

	private createurl(text: string, key: string, lang: string, speed: number) {
		const url = new URL('/translate_tts', this.host);
		url.search = new URLSearchParams({
			ie: 'UTF-8',
			q: text,
			tl: lang || 'en',
			total: '1',
			idx: '0',
			textlen: text.length.toString(),
			tk: this.token(text, key),
			client: 't',
			prev: 'input',
			ttsspeed: speed.toString() || '1'
		}) as any;
		return url.href;
	};

	async create(text: string, lang: string, speed: number, timeout: number = 10000) {
		const key = await this.fetchKeys(timeout);
		return this.createurl(text, key, lang, speed);
	}
}

export default new GoogleTTS;